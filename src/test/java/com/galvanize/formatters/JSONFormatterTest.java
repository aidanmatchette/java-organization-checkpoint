package com.galvanize.formatters;

import com.galvanize.Booking;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class JSONFormatterTest {
    private Booking book;
    private JSONFormatter json;

    @BeforeEach
    void setUp() {
        book = new Booking(Booking.RoomType.SUITE, "111", "08:30am", "11:00am");
        json = new JSONFormatter();
    }

    @Test
    void validBookingInput() {

        String expected = "{\n" +
                "  \"type\": \"Suite\",\n" +
                "  \"roomNumber\": 111,\n" +
                "  \"startTime\": \"08:30am\",\n" +
                "  \"endTime\": \"11:00am\"\n" +
                "}";

        String actual = json.format(book);

        assertEquals(expected, actual);
    }

    @Test
    void invalidJSONOutput() {

        String incorrect = "{" +
                "  \"type\": \"%s\",\n" +
                "  \"roomNumber\": \"%s\",\n" +
                "  \"startTime\": \"%s\",\n" +
                "  \"endTime\": \"%s\",\n" +
                "}";

        assertNotEquals(incorrect, json.format(book));
    }
}