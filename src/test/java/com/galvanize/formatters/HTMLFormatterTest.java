package com.galvanize.formatters;

import com.galvanize.Booking;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HTMLFormatterTest {
    private Booking book;
    private HTMLFormatter html;

    @BeforeEach
    void setUp() {
        book = new Booking(Booking.RoomType.CONFERENCE, "111", "08:30am", "11:00am");
        html = new HTMLFormatter();
    }

    @Test
    void validBookingInput() {

        String expected =  "<dl>\n" +
            "  <dt>Type</dt><dd>Conference Room</dd>\n" +
            "  <dt>Room Number</dt><dd>111</dd>\n" +
            "  <dt>Start Time</dt><dd>08:30am</dd>\n" +
            "  <dt>End Time</dt><dd>11:00am</dd>\n" +
            "</dl>";


        String actual = html.format(book);

        assertEquals(expected, actual);
    }

    @Test
    void invalidHTMLOutput() {

        String incorrect =  "<dl>" +
                "  <dt>Type</dt><dd>Conference Room</dd>" +
                "  <dt>Room Number</dt><dd>111</dd>" +
                "  <dt>Start Time</dt><dd>08:30am</dd>" +
                "  <dt>End Time</dt><dd>11:00am</dd>" +
                "</dl>";

        assertNotEquals(incorrect, html.format(book));
    }
}