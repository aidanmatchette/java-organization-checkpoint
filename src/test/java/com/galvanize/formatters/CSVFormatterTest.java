package com.galvanize.formatters;

import com.galvanize.Booking;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CSVFormatterTest {
    private Booking book;
    private CSVFormatter csv;

    @BeforeEach
    void setUp() {
        book = new Booking(Booking.RoomType.AUDITORIUM, "111", "08:30am", "11:00am");
        csv = new CSVFormatter();
    }

    @Test
    void validBookingInput() {

        String expected =  "type,room number,start time,end time\n" +
                "Auditorium,111,08:30am,11:00am";

        String actual = csv.format(book);

        assertEquals(expected, actual);
    }

    @Test
    void invalidCSVOutput() {

        String incorrect =  "type,room number,start time,end time" +
                "Auditorium,111,08:30am,11:00am";

        assertNotEquals(incorrect, csv.format(book));
    }

}