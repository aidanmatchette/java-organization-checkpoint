package com.galvanize;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.awt.print.Book;

import static org.junit.jupiter.api.Assertions.*;

class BookingTest {
    private String bookingCode;
    private Booking book;

    @BeforeEach
    void setUp() {
        bookingCode = "s111-08:30am-11:00am";
        book = Booking.parse(bookingCode);
    }

    @Test
    void parse() {

        String expectedToString = "Booking{" +
                "roomType=SUITE" +
                ", roomNumber='111" + '\'' +
                ", startTime='08:30am" +  '\'' +
                ", endTime='11:00am"  + '\'' +
                '}';

        assertEquals(expectedToString, book.toString());
    }

    @Test
    void getRoomType() {

        Booking.RoomType expected = Booking.RoomType.SUITE;
        Booking.RoomType actual = book.getRoomType();

        assertEquals(expected, actual);
    }

    @Test
    void getRoomNumber() {

        String expected = "111";
        String actual = book.getRoomNumber();

        assertEquals(expected,actual);
    }

    @Test
    void getStartTime() {

        String expected = "08:30am";
        String actual = book.getStartTime();

        assertEquals(expected,actual);
    }

    @Test
    void getEndTime() {

        String expected = "11:00am";
        String actual = book.getEndTime();

        assertEquals(expected,actual);
    }
}