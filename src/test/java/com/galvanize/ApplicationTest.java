package com.galvanize;

import com.galvanize.formatters.Formatter;
import com.galvanize.formatters.HTMLFormatter;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ApplicationTest {

    PrintStream original;
    ByteArrayOutputStream outContent;
    String[] args;

    // This block captures everything written to System.out
    @BeforeEach
    public void setOut() {

        args = new String[]{"r111-08:30am-11:00am", "html"};
        original = System.out;
        outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
    }

    // This block resets System.out to whatever it was before
    @AfterEach
    public void restoreOut() {
        System.setOut(original);
    }

    @Test
    public void getFormatter() {
        // Write your tests here
        // outContent.toString() will give you what your code printed to System.out

        Formatter actual = Application.getFormatter(args[1]);

        assertTrue(actual instanceof HTMLFormatter);
    }

    @Test
    public void applicationRuns() {
        Application.main(args);
        String actual = outContent.toString();


        String expected =  "<dl>\n" +
                "  <dt>Type</dt><dd>Conference Room</dd>\n" +
                "  <dt>Room Number</dt><dd>111</dd>\n" +
                "  <dt>Start Time</dt><dd>08:30am</dd>\n" +
                "  <dt>End Time</dt><dd>11:00am</dd>\n" +
                "</dl>\n";
        assertEquals(expected, actual);
    }

}
