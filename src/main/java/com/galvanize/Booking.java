package com.galvanize;

import java.util.HashMap;
import java.util.Map;

public class Booking {
    
    public enum RoomType {
        CONFERENCE, AUDITORIUM, SUITE, CLASSROOM
    }

    private RoomType roomType;
    private String roomNumber;
    private String startTime;
    private String endTime;
    private final Map<RoomType, String> enumToString = new HashMap<>() {{
        put(RoomType.AUDITORIUM, "Auditorium");
        put(RoomType.CONFERENCE, "Conference Room");
        put(RoomType.CLASSROOM, "Classroom");
        put(RoomType.SUITE, "Suite");
    }};


    @Override
    public String toString() {
        return "Booking{" +
                "roomType=" + roomType +
                ", roomNumber='" + roomNumber + '\'' +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                '}';
    }

    public Booking(RoomType roomType, String roomNumber, String startTime, String endTime) {
        this.roomType = roomType;
        this.roomNumber = roomNumber;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public static Booking parse(String rawData) {
        String[] data = rawData.split("-");
        char firstChar = data[0].charAt(0);
        String roomNum = data[0].substring(1);
        String start = data[1];
        String end = data[2];

        RoomType type = null;
        switch (firstChar) {
           case 'r':
               type = RoomType.CONFERENCE;
               break;
           case 'a':
               type = RoomType.AUDITORIUM;
               break;
           case 's':
               type = RoomType.SUITE;
               break;
           case 'c':
               type = RoomType.CLASSROOM;
               break;
        }

       return new Booking(type, roomNum, start, end);
    }

    public RoomType getRoomType() {
        return this.roomType;
    }

    public String getRoomTypeString() {
        return enumToString.get(this.roomType);
    }
    public String getRoomNumber() {
        return roomNumber;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }

}
