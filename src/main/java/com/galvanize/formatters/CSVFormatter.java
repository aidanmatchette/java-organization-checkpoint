package com.galvanize.formatters;

import com.galvanize.Booking;

public class CSVFormatter implements Formatter {
    @Override
    public String format(Booking bookingType) {
        return String.format("type,room number,start time,end time\n" +
                "%s,%s,%s,%s", bookingType.getRoomTypeString(), bookingType.getRoomNumber(), bookingType.getStartTime(), bookingType.getEndTime());
    }
}
