package com.galvanize.formatters;

import com.galvanize.Booking;

public class JSONFormatter implements Formatter{

    @Override
    public String format(Booking bookingType) {

        return String.format(
                "{\n" +
                        "  \"type\": \"%s\",\n" +
                        "  \"roomNumber\": %s,\n" +
                        "  \"startTime\": \"%s\",\n" +
                        "  \"endTime\": \"%s\"\n" +
                        "}"
                , bookingType.getRoomTypeString(), bookingType.getRoomNumber(), bookingType.getStartTime(), bookingType.getEndTime());
    }
}
